package app

import error.WebClientException
import groovy.json.JsonSlurper

/**
 * Created by user on 22/12/2015.
 */
class ChuckNorrisRestClient {

    final String WEB_SUCCESS = 'success'
    String url

    String getJoke(){
        def response = new JsonSlurper().parseText url.toURL().text

        if(response.type != WEB_SUCCESS){
            throw new WebClientException("Request failed. Returned status was ${response.type}")
        }
        response?.value?.joke
    }
}
