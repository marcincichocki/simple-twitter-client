package app

import groovy.time.TimeCategory
import groovy.time.TimeDuration
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import twitter.Connector

import java.util.logging.FileHandler

/**
 * Created by user on 18/12/2015.
 */
@Slf4j
@Component
class Runner implements CommandLineRunner {

    final int TWEET_MAX_LEN = 140

    @Autowired
    Connector connector

    @Autowired
    ChuckNorrisRestClient chuckNorrisRestClient

    @Override
    void run(String... args) throws Exception {
        def timeStart = new Date()
        String newStatus = "${chuckNorrisRestClient.getJoke()} #groovy #api #java #joke #chucknorris #chuck #norris"
        if(newStatus.length() > TWEET_MAX_LEN){
            newStatus = newStatus[0..TWEET_MAX_LEN-1]
        }
        connector.updateStatus(newStatus)

        def timeStop = new Date()
        TimeDuration duration = TimeCategory.minus(timeStop, timeStart)

        log.info "Execution time was $duration. Status set to $newStatus"
    }
}
