package app

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import twitter.Connector

/**
 * Created by user on 18/12/2015.
 */
@SpringBootApplication
class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application, args)
    }

    @Bean
    Connector connector(){
        new Connector()
    }

    @Bean
    ChuckNorrisRestClient chuckNorrisRestClient(@Value('${url.chucknorris.random}')url){
        new ChuckNorrisRestClient(url: url)
    }
}
