package twitter

import twitter4j.Status
import twitter4j.Twitter
import twitter4j.TwitterFactory

/**
 * Created by user on 18/12/2015.
 */
class Connector {

    void updateStatus(String statusUpdate){
        Twitter twitter = TwitterFactory.getSingleton()
        Status status = twitter.updateStatus(statusUpdate)
        println "Successfully updated the status to [${status.getText()}]."
    }
}
